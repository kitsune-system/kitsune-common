import { expect } from 'chai';

import { Bucket } from './bucket';
import { Valve } from './valve';

describe('Valve', () => {
  it('should work', () => {
    const bucket = Bucket();

    const valve = Valve();
    valve.onOutput(bucket);

    expect(bucket.empty()).to.deep.equal([]);

    valve(100);
    valve(200);

    expect(bucket.empty()).to.deep.equal([]);

    valve.open(true);
    valve(300);
    valve(400);

    expect(bucket.empty()).to.deep.equal([300, 400]);

    valve.open(false);
    valve(500);
    valve(600);

    expect(bucket.empty()).to.deep.equal([]);
  });
});
