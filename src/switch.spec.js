import { expect } from 'chai';

import { Bucket } from './bucket';
import { ListCollector } from './collector';
import { Switch } from './switch';

const predicates = {
  even: ({ input, onOutput }) => onOutput(input !== null && input % 2 === 0),
  negative: ({ input, onOutput }) => onOutput(input < 0),
  small: ({ input, onOutput }) => onOutput(input > 0 && input < 10),
  large: ({ input, onOutput }) => onOutput(input >= 10),
  positive: ({ input, onOutput }) => onOutput(input > 0),
};

describe('Switch', () => {
  it('should work', done => {
    const sw = Switch();

    const open = ({ onOutput }) => {
      sw.open({ onOutput: id => {
        sw.getConditionId({ input: id, onOutput: conditionId => {
          const output = sw[id];
          const condition = sw[conditionId];

          onOutput({ onOutput: output, onCondition: condition });
        } });
      } });
    };

    const buckets = { default: Bucket() };
    const collect = ListCollector();

    const predicateNames = Object.keys(predicates);
    predicateNames.forEach(name => {
      const openC = collect();

      open({ onOutput: ({ onOutput, onCondition }) => {
        const predicate = predicates[name];

        const bucket = Bucket();
        buckets[name] = bucket;

        onOutput(bucket);
        onCondition(predicate);

        openC();
      } });
    });

    collect.done(() => {
      sw.onDefault(buckets.default);

      sw.input('missing');

      for(let i = -5; i < 15; i++)
        sw.input(i);

      sw.input('another one');

      expect(buckets.even.empty()).to.deep.equal([-4, -2, 0, 2, 4, 6, 8, 10, 12, 14]);
      expect(buckets.negative.empty()).to.deep.equal([-5, -4, -3, -2, -1]);
      expect(buckets.small.empty()).to.deep.equal([1, 2, 3, 4, 5, 6, 7, 8, 9]);
      expect(buckets.large.empty()).to.deep.equal([10, 11, 12, 13, 14]);
      expect(buckets.positive.empty()).to.deep.equal([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]);
      expect(buckets.default.empty()).to.deep.equal(['missing', 'another one']);

      done();
    });
  });
});
