import { expect } from 'chai';
import { fake } from 'sinon';

import { copies, namedCopies } from '@gamedevfox/katana';

import { Bucket } from './bucket';
import { Collector, ListCollector } from './collector';

describe('Collector', () => {
  it('should work', () => {
    const collect = Collector();

    const first = collect('first');
    const another = collect('another');
    const last = collect('last');

    first(1);

    const myFake = fake();
    collect.done(myFake);

    expect(myFake.args).to.deep.equal([]);

    last(123);
    expect(myFake.args).to.deep.equal([]);

    another('one');
    expect(myFake.args).to.deep.equal([[{
      another: 'one', first: 1, last: 123,
    }]]);
  });

  it('should work using nameless collectors', () => {
    const collect = Collector();

    const first = collect();
    const another = collect();
    const last = collect();

    first();

    const myFake = fake();
    collect.done(myFake);

    expect(myFake.args).to.deep.equal([]);

    last();
    expect(myFake.args).to.deep.equal([]);

    another();
    expect(myFake.args).to.deep.equal([[{}]]);
  });

  it('should work if all values are already collected', () => {
    const collect = Collector();

    collect('first')(1);
    collect('another')('one');
    collect('last')(123);

    const myFake = fake();
    collect.done(myFake);
    expect(myFake.args).to.deep.equal([[{
      another: 'one', first: 1, last: 123,
    }]]);
  });

  it('list example', () => {
    const collect = ListCollector();

    const first = collect();
    const another = collect('names-are-ignored');
    const last = collect();

    first(1);

    const bucket = Bucket();
    collect.done(bucket);

    expect(bucket.empty()).to.deep.equal([]);

    last(123);
    expect(bucket.empty()).to.deep.equal([]);

    another('one');
    expect(bucket.empty()).to.deep.equal([[1, 123, 'one']]);
  });

  it('example with copies', () => {
    const collector = ListCollector();
    const collectors = copies(3, collector);

    const check = () => collectors.shift()();

    const bucket = Bucket();
    collector.done(bucket);

    check();
    expect(bucket.empty()).to.deep.equal([]);

    check();
    expect(bucket.empty()).to.deep.equal([]);

    check();
    expect(bucket.empty()).to.deep.equal([[undefined, undefined, undefined]]);
  });

  it('example use with namedCopies', () => {
    const collector = Collector();
    const collectors = namedCopies(3, collector);

    const bucket = Bucket();
    collector.done(bucket);

    collectors.b();
    expect(bucket.empty()).to.deep.equal([]);

    collectors.c();
    expect(bucket.empty()).to.deep.equal([]);

    collectors.a();
    expect(bucket.empty()).to.deep.equal([{
      a: undefined, b: undefined, c: undefined,
    }]);
  });

  it('lateName', () => {
    const collect = Collector();

    const first = collect('first');
    const another = collect('earlyName');
    const last = collect();

    first(1);

    const myFake = fake();
    collect.done(myFake);

    expect(myFake.args).to.deep.equal([]);

    last('lateName', 123);
    expect(myFake.args).to.deep.equal([]);

    another('overrideEarlyName', 'one');
    expect(myFake.args).to.deep.equal([[{
      first: 1, lateName: 123, overrideEarlyName: 'one',
    }]]);
  });
});
