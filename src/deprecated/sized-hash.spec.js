import { expect } from 'chai';

import {
  truncateByte, ensureBit, capByte, markSize, measureSize,
} from './sized-hash';

describe('sized-hash', () => {
  describe('truncateByte', () => {
    it('should work', () => {
      expect(truncateByte(255, 1)).to.equal(1);
      expect(truncateByte(255, 2)).to.equal(3);
      expect(truncateByte(255, 3)).to.equal(7);
      expect(truncateByte(255, 4)).to.equal(15);
      expect(truncateByte(255, 5)).to.equal(31);
      expect(truncateByte(255, 6)).to.equal(63);
      expect(truncateByte(255, 7)).to.equal(127);
      expect(truncateByte(255, 8)).to.equal(255);

      expect(truncateByte(100, 3)).to.equal(4);
      expect(truncateByte(123, 4)).to.equal(11);
      expect(truncateByte(234, 5)).to.equal(10);
    });
  });

  describe('ensureBit', () => {
    it('should work', () => {
      expect(ensureBit(0, 1)).to.equal(1);
      expect(ensureBit(0, 2)).to.equal(2);
      expect(ensureBit(0, 3)).to.equal(4);
      expect(ensureBit(0, 4)).to.equal(8);
      expect(ensureBit(0, 5)).to.equal(16);
      expect(ensureBit(0, 6)).to.equal(32);
      expect(ensureBit(0, 7)).to.equal(64);
      expect(ensureBit(0, 8)).to.equal(128);

      expect(ensureBit(100, 5)).to.equal(116);
      expect(ensureBit(123, 3)).to.equal(127);
      expect(ensureBit(234, 4)).to.equal(234);
    });
  });

  describe('capByte', () => {
    it('should work', () => {
      expect(capByte(85, 1)).to.equal(1);
      expect(capByte(85, 2)).to.equal(3);
      expect(capByte(85, 3)).to.equal(5);
      expect(capByte(85, 4)).to.equal(13);
      expect(capByte(85, 5)).to.equal(21);
      expect(capByte(85, 6)).to.equal(53);
      expect(capByte(85, 7)).to.equal(85);
      expect(capByte(85, 8)).to.equal(213);

      expect(capByte(100, 5)).to.equal(20);
      expect(capByte(123, 6)).to.equal(59);
      expect(capByte(234, 7)).to.equal(106);
    });
  });

  describe('markSize', () => {
    it('should work', () => {
      const example = () => Buffer.from('55ffffff', 'hex');
      expect(markSize(example(), 1).toString('hex')).to.equal('01ffffff');
      expect(markSize(example(), 2).toString('hex')).to.equal('03ffffff');
      expect(markSize(example(), 3).toString('hex')).to.equal('05ffffff');
      expect(markSize(example(), 4).toString('hex')).to.equal('0dffffff');
      expect(markSize(example(), 5).toString('hex')).to.equal('15ffffff');
      expect(markSize(example(), 6).toString('hex')).to.equal('35ffffff');
      expect(markSize(example(), 7).toString('hex')).to.equal('55ffffff');
      expect(markSize(example(), 8).toString('hex')).to.equal('d5ffffff');

      const example2 = () => Buffer.from('89abcdef', 'hex');
      expect(markSize(example2(), 28).toString('hex')).to.equal('09abcdef');
      expect(markSize(example2(), 29).toString('hex')).to.equal('19abcdef');
      expect(markSize(example2(), 31).toString('hex')).to.equal('49abcdef');
    });
  });

  describe('measureSize', () => {
    it('should work', () => {
      const hexHash = hex => Buffer.from(hex, 'hex');
      expect(measureSize(hexHash('01ffffff'))).to.equal(25);
      expect(measureSize(hexHash('03ffffff'))).to.equal(26);
      expect(measureSize(hexHash('05ffffff'))).to.equal(27);
      expect(measureSize(hexHash('0dffffff'))).to.equal(28);
      expect(measureSize(hexHash('15ffffff'))).to.equal(29);
      expect(measureSize(hexHash('35ffffff'))).to.equal(30);
      expect(measureSize(hexHash('55ffffff'))).to.equal(31);
      expect(measureSize(hexHash('d5ffffff'))).to.equal(32);

      expect(measureSize(hexHash('09abcdef'))).to.equal(28);
      expect(measureSize(hexHash('19abcdef'))).to.equal(29);
      expect(measureSize(hexHash('49abcdef'))).to.equal(31);
    });
  });
});
