import { expect } from 'chai';

import { deepHashEdge, hash, hashList, pseudoRandom } from './hash';
import { RANDOM, READ, WRITE } from './index';

describe('hash', () => {
  it('hashList', () => {
    expect(hashList([
      'AIijUH1v1Jxo6gBDm5rwI4Or80AwPum9At1AWbzw5Lw=',
      'PFdJkeCnbCZzBF+bLC0Fb7vCRwbFKfv8hBwz6wH7yjk=',
    ])).to.equal('fN/7GeSZDoqSPpyB7Ma9qGfq2fLdZs714JLUlm2HI3I=');
  });

  it('hash.string', () => {
    expect(hash.string('')).to.equal('gRB6Zjl2BgkHSbynpo4AH4MEBlCFGdQbaXKrUsNyRQs=');
    expect(hash.string('Hello World')).to.equal('mHjKZqSLLbx1kM953E3UyNW/5oWNOBhNPbNtD9dTJXk=');
    expect(hash.string('こんにちは')).to.equal('2vG7X2iRbp+0/ff82VOXOD2ZzvShhR+wSkxTiJk+eSA=');

    expect(hash.string(Buffer.from(''))).to.equal('gRB6Zjl2BgkHSbynpo4AH4MEBlCFGdQbaXKrUsNyRQs=');
    expect(hash.string(Buffer.from('Hello World'))).to.equal('mHjKZqSLLbx1kM953E3UyNW/5oWNOBhNPbNtD9dTJXk=');
    expect(hash.string(Buffer.from('こんにちは'))).to.equal('2vG7X2iRbp+0/ff82VOXOD2ZzvShhR+wSkxTiJk+eSA=');

    expect(hash.string(Buffer.from('', 'utf8'))).to.equal('gRB6Zjl2BgkHSbynpo4AH4MEBlCFGdQbaXKrUsNyRQs=');
    expect(hash.string(Buffer.from('Hello World', 'utf8'))).to.equal('mHjKZqSLLbx1kM953E3UyNW/5oWNOBhNPbNtD9dTJXk=');
    expect(hash.string(Buffer.from('こんにちは', 'utf8'))).to.equal('2vG7X2iRbp+0/ff82VOXOD2ZzvShhR+wSkxTiJk+eSA=');
  });

  it('hash.edge', () => {
    expect(hash.edge(['first', 'second'])).to.equal('uBihcDF5ROpBoGKiRDufReu4HINwSRGjYtfOv/bi9JA=');

    expect(() => {
      hash.edge(['hello']);
    }).to.throw('`edge` must have 2 elements, instead had 1');

    expect(() => {
      hash.edge(['hello', 'one', 'more']);
    }).to.throw('`edge` must have 2 elements, instead had 3');

    expect(() => {
      hash.edge([123, null]);
    }).to.throw('Both `head` and `tail` must be strings: [123,null]');
  });

  it('hash.set', () => {
    expect(hash.set([])).to.equal('HVUB/D3kwRxj6o/5d6PKrYOKhzYeC/jMj1GPpln6IPs=');
    expect(hash.set(['SINGLE'])).to.equal('M6cXjndhhgeJehVswLPjH7TNUNboa1mJ5xxGRd/vh6k=');
    expect(hash.set(['ALPHA', 'BETA', 'OMEGA'])).to.equal('9hli1ScInk7BUr4+p0KO7O0HsEOV2u6HTtJaAjEEW1I=');
    expect(hash.set(['OMEGA', 'BETA', 'ALPHA'])).to.equal('9hli1ScInk7BUr4+p0KO7O0HsEOV2u6HTtJaAjEEW1I=');
  });

  it('hash.list', () => {
    expect(hash.list([])).to.equal('NZn+DEupxPkW+ajihacCWsU7dC4YNbQl1Rs2NsCR6DU=');
    expect(hash.list(['SINGLE'])).to.equal('gisDLFchSv5qxEk9LUGhkJP3RAuC+/eEO0IOakT8jII=');
    expect(hash.list(['ALPHA', 'BETA', 'OMEGA'])).to.equal('xbiDjY7X6eDzDZNFtY2F93cUhbkOKrxLdvR62QSzCcA=');
    expect(hash.list(['OMEGA', 'BETA', 'ALPHA'])).to.equal('YtW8/3qtU6qAmsFj97QmFXaP48MtDaSSzGymX1ijgcc=');
  });

  it('hash.map', () => {
    expect(hash.map({})).to.equal('HVUB/D3kwRxj6o/5d6PKrYOKhzYeC/jMj1GPpln6IPs=');
    expect(hash.map({
      ALPHA: 'ONE',
      BETA: 'TWO',
      OMEGA: 'LAST',
    })).to.equal('R3ljC5HMy3na3LkSPZzI7gB/c9tvg5jWRA4bQQ8o/uA=');
    expect(hash.map({
      OMEGA: 'LAST',
      BETA: 'TWO',
      ALPHA: 'ONE',
    })).to.equal('R3ljC5HMy3na3LkSPZzI7gB/c9tvg5jWRA4bQQ8o/uA=');
  });

  it('hash', () => {
    expect(hash('This is a String')).to.equal('uiIaAOR2OAPV+LjfvsMn8HEWNBeoK9a7sYS24DkbAyM=');
    expect(hash('EDGE_HEAD', 'EDGE_TAIL')).to.equal('1j/KS2O88OVYy9qv2oOYYwKsFQvqMrBpkJhsLSzda9k=');
    expect(hash(['THIS', 'IS', 'A', 'SET'])).to.equal('TXhwOBFBuNqU2A79KHPrsOsuFVu0IDWJiPwix0w46y8=');
    expect(hash({
      THIS: 'IS',
      A: 'MAP',
    })).to.equal('BGLKs73FypI/TlFG0U7bBZa7ThRMNGDsBMZhK7A77CU=');
  });

  describe('deepHashEdge', () => {
    it('should edge hash a nested array of nodes', () => {
      const normal = hash.edge([RANDOM, hash.edge([READ, WRITE])]);
      const deep = deepHashEdge(RANDOM, [READ, WRITE]);

      expect(deep).to.equal(normal);
    });
  });

  it('pseudoRandom', () => {
    const random = pseudoRandom(RANDOM);

    expect(random()).to.equal('MF7Yc6gRuVWrfWR0Cs9VKAoqTcVsMfdWeXmcjAE3goU=');
    expect(random()).to.equal('ZE5nfIvBE892Mh0ZiMAF6bYN13Ec8xovE2S15y9XrjU=');
    expect(random()).to.equal('PFdJkeCnbCZzBF+bLC0Fb7vCRwbFKfv8hBwz6wH7yjk=');
    expect(random()).to.equal('j36ZUZQjVbthdwf3jL5eubGhZTYY/93+AbGyTivU2TY=');
    expect(random()).to.equal('uo587Bpkiy1hLGJ+JIwO4QZxrHKrdcVA3gY1FyWyb/c=');
  });
});
