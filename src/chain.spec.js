import { expect } from 'chai';

import { chain } from './chain';

const double = ({ input, onOutput }) => {
  onOutput(input * 2);
};

const over9000 = ({ number, result }) => {
  result(Math.abs(number) + 9001);
};

const increment = ({ input, onOutput }) => {
  onOutput(input + 1);
};

describe('chain', () => {
  it('should work', () => {
    chain(
      ({ onOutput }) => double({ input: 123, onOutput }),
      ({ input, onOutput }) => {
        expect(input).to.equal(246);
        over9000({ number: input, result: onOutput });
      },
      ({ input, onOutput }) => {
        expect(input).to.equal(9247);
        increment({ input, onOutput });
      },
      ({ input }) => {
        expect(input).to.equal(9248);
      },
    );
  });

  it('should work with initial input', () => {
    chain(987,
      ({ input, onOutput }) => {
        expect(input).to.equal(987);
        double({ input, onOutput });
      },
      ({ input, onOutput }) => {
        expect(input).to.equal(1974);
        over9000({ number: input, result: onOutput });
      },
      ({ input, onOutput }) => {
        // An async callback just to mix things up :)
        setTimeout(() => {
          expect(input).to.equal(10975);
          increment({ input, onOutput });
        }, 0);
      },
      ({ input }) => {
        expect(input).to.equal(10976);
      },
    );
  });
});
