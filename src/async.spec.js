import { expect } from 'chai';

import { copies } from '@gamedevfox/katana';

import { Async } from './async';
import { ListCollector } from './collector';

describe('Async', () => {
  it('should work', done => {
    const collect = ListCollector();

    const [collectA, collectB] = copies(2, collect);
    collect.done(() => done());

    const system = Async();

    let value = 123;

    system.onOutput(newValue => {
      value = newValue;
      expect(value).to.equal(456);
      collectA();
    });

    system.input(456);

    expect(value).to.equal(123);
    collectB();
  });
});
