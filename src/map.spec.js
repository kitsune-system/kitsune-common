import { expect } from 'chai';

import { Map } from './map';

describe('Map', () => {
  it('should work', done => {
    const map = Map();

    map.set({ id: 'ALPHA', value: 'BETA' });
    map.get({ input: 'ALPHA', onOutput: value => {
      expect(value).to.equal('BETA');
      done();
    } });
  });
});
